

const txtFName = document.querySelector('#txt-fname');
const txtLName = document.querySelector('#txt-lname');
const spanFullName = document.querySelector('#span-fullname')


txtFName.addEventListener('keyup',updateLabel)
txtLName.addEventListener('keyup',updateLabel)

function updateLabel(event){
    spanFullName.innerHTML = `${txtFName.value} ${txtLName.value}`
}
