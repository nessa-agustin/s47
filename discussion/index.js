// alert('hello world')

// console.log(document.querySelector("#txt-fname"))
// console.log(document)


// DOM Manipulation

const txtFName = document.querySelector('#txt-fname');
// const txtLName = document.querySelector('#txt-lname');
const spanFullName = document.querySelector('#span-fullname')


console.log(txtFName)
console.log(spanFullName)

/* 
    Event:
        click, hover, keypress, keyup and many others

    Event Listeners:
        allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.

    Syntax:
        selectedElement.addEventListener('event',function)

*/

// txtFName.addEventListener('keyup',(event) => {
//     spanFullName.innerHTML = txtFName.value
//     console.log(event)
//     console.log(event.target)
//     console.log(event.target.value)

// })

txtFName.addEventListener('keyup',printFirstName)

function printFirstName(event){
    spanFullName.innerHTML = txtFName.value
}

const lblFname = document.querySelector('#lbl-txtFname')

lblFname.addEventListener('click',(e) => {
    console.log(e)
    alert('You clicked the First Name label')
})


/* 


*/